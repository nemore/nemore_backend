import yaml
import os
import logging
from pathlib import Path

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.CRITICAL)
logger = logging.getLogger(__name__)

from flask import Flask
from flask import Response
from flask import request
from flask import jsonify

from app import logging
logger = logging.getLogger(__name__)

# Define the WSGI application object
app = Flask(__name__)

users = {}

#########################################
# SERVER CONFIG
#########################################
_server_config_file = "etc/server-config.yaml"

# Default server configuration
server_config = {
    "app": {
        "host": "0.0.0.0",
        "port": 8888
    },
    "database": {
        "type": "sqlite",
        "host": "127.0.0.1",
        "port": 3306,
        "database": "nemore",
        "username": "root",
        "password": "default",
        # SQLite - path to database
        "storage": "nemore.sqlite3"
    }
}

# If config file is provided for server, use that instead of default
if os.path.exists(_server_config_file):
    with open(_server_config_file, "r") as config_file:
        server_config = yaml.load(config_file)

from app import database

#########################################
# ROUTES
#########################################
from app import routes


def run():

    database.initial_migration()

    # Run the app
    app.run(
        host=server_config["app"]["host"], port=server_config["app"]["port"])

    app.run(host='0.0.0.0', port=8080)