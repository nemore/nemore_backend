import json
from pathlib import Path
import time
DATABASE_FILE = "nemore.json"


class Fish:

    def __init__(self, name, location):
        self._name = name
        self._deployed = deployed
        self._location = location
        self._longitude = longitude
        self._latitude = latitude

        self._ldr_val = 0

        self._acc_x_val = 0.0
        self._acc_y_val = 0.0
        self._acc_z_val = 0.0

        self._gyro_x_val = 0.0
        self._gyro_y_val = 0.0
        self._gyro_z_val = 0.0

        self._mag_x_val = 0.0
        self._mag_y_val = 0.0
        self._mag_z_val = 0.0            

    def to_json():
        r = f'{{"name":{self._name}, "location":{{"longitude":{self._longitude}, "latitude":{self._latitude}}},"data":{{"ldr":{{"ldr":{self._ldr_val}}},"ace":{{"x":{_acc_x_val}, "y":{_acc_y_val}, "z":{_acc_z_val}}},"gyro":{{"x":{_gyro_x_val}, "y":{_gyro_y_val}, "z":{_gyro_z_val}}},mag:{{"x":{_mag_x_val}, "y":{_mag_y_val}, "z":{_mag_z_val}}}}}}}'
        return json.loads(r)

class Point:

    def __init__(self, longitude, latitude):
        self._longitude = longitude
        self._latitude = latitude


def initial_migration():
    if not Path(DATABASE_FILE).exists():
        with open(Path(DATABASE_FILE), "w") as f:
            f.write("[]")


def _read_database():
    with open(DATABASE_FILE, "r") as f:
        database = json.loads(f.read())
    return database


def _write_database(database):
    with open(DATABASE_FILE, "w") as f:
        f.write(json.dumps(database))


def create_fish(fish):
    database = _read_database()
    fish["id"] = get_last_id(database)
    fish["deployed_date"] = int(time.time())
    database.append(fish)
    _write_database(database)

def update_fish(id, fish):
    database = _read_database()
    if len(database) == 0:
        return None

    fish_to_update = None
    for _fish in database:        
        if int(_fish["id"]) == int(id):
            fish_to_update = fish
    
    fish_to_update = fish
    _write_database(database)


def get_last_id(database):
    max_id = 0;
    print(len(database))
    if len(database) == 0:
        return 1
    
    for fish in database:
        if fish["id"] > max_id:
            max_id = fish["id"]    
    return max_id+1


def get_all_fishes():
    return _read_database()

def get_fish_by_id(id):    
    database = _read_database()
    if len(database) == 0:
        return None

    for fish in database:        
        if int(fish["id"]) == int(id):
            return fish
    return None

