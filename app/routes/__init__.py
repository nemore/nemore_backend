from flask import jsonify
from flask import request

from app import app

from app.database import create_fish, get_all_fishes,get_fish_by_id,update_fish


@app.route("/", methods=["GET"])
def home():
    """ Home handler """
    return jsonify(message="Welcome!"), 200


@app.route("/fish", methods=["GET"])
def get_fishes():
    users = None
    try:
        users = get_all_fishes()
        print(users)
        if not users:
            return jsonify("No hay peces"), 400
    except Exception as e:
        print(e)

    return jsonify(f"{users}"), 200

@app.route("/fish/<fish_id>", methods=["GET"])
def get_fish(fish_id):   
    try:                
        fish = get_fish_by_id(fish_id)
        if fish is None:
            return jsonify("Fish does not exist"), 400
    except Exception as e:
        print(e)

    return jsonify(f"{fish}"), 200


@app.route("/fish/deploy", methods=["POST"])
def fish_deploy():

    if request.json: 
        try:
            fish_name = request.json.get("name")
            fish_longitude = request.json.get("longitude")
            fish_latitude = request.json.get("latitude")            
            f = {
                "name":fish_name, 
                "deployed":True, 
                "longitude":fish_longitude, 
                "latitude":fish_latitude,
                "data":[
                    {
                    "ldr_val":0,

                    "acc_x_val":0.0,
                    "acc_y_val":0.0,
                    "acc_z_val":0.0,

                    "gyro_x_val":0.0,
                    "gyro_y_val":0.0,
                    "gyro_z_val":0.0,

                    "mag_x_val":0.0,
                    "mag_y_val":0.0,
                    "mag_z_val":0.0 
                    }
                ]
            }
            create_fish(f)
        except Exception as e:
            print(e)
    else:
        return jsonify(message="No data sent!"), 400
        
    return jsonify("Pez deployeado"), 200


@app.route("/fish/update", methods=["POST"])
def fish_update():
    if request.json: 
        try:
            fish_id = request.json.get("fish_id")
            data = request.json.get("data")
            print(fish_id)
            fish = get_fish_by_id(fish_id)
            fish["data"].extend(data)
            update_fish(fish_id, fish)
        except Exception as e:
            print(e)

    return jsonify("Pez updateado"), 200


@app.route("/fish/retrieve", methods=["GET"])
def fish_retrieve():
    return jsonify("Pez retrieveado"), 200


@app.route("/fish/state", methods=["GET"])
def fish_state():
    return jsonify("Pez stateado"), 200


@app.route("/map", methods=["GET"])
def map():
    """ """
    id = request.args.get('userid')
    # user = users.get(id, None)

    # if user is None:
    #     return jsonify(message="user does not exists!"), 400

    # return jsonify(message="Greetings {}!".format(users[id]["name"])), 200
    return jsonify(""), 200


@app.route("/user/<user_id>", methods=["GET"])
def get_user(user_id):
    """ Get user handler, the user_id will be passed as parameter and it
        returns the user data

        eg. http://localhost:8080/user/1
    """

    # user = users.get(user_id, None)
    # if user is None:
    #     return jsonify(message="user not found!"), 400

    # return jsonify(message="{}!".format(user)), 200
    return jsonify(""), 200


@app.route("/user", methods=["POST"])
def add_user():
    """ Adds a user to the "database" """

    # Get user data from post form
    # user_id = request.form["id"]
    # user_name = request.form["name"]
    # user_age = request.form["age"]

    # user = users.get(user_id, None)
    # if user is not None:
    #     return jsonify(
    #         message="user with id {} already exists!".format(user_id)), 400

    # # Adds the user to the dump database
    # user = {"name": user_name, "age": user_age}
    # users[user_id] = user

    # return jsonify(message="user added succesfully"), 200

    return jsonify(""), 200


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return jsonify({"message": "Error, could not found!"}), 404