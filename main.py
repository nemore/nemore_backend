import app

import os
os.environ['BASE_PATH'] = os.path.abspath(os.path.dirname(__file__))

if __name__ == "__main__":
    app.run()